﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Extraexempel_2___Arv
{
    class Input
    {
        public static MouseState mus = Mouse.GetState();
        public static MouseState gammalMus = Mouse.GetState();
        public static KeyboardState tangentbord = Keyboard.GetState();
        public static KeyboardState gammaltTangentbord = Keyboard.GetState();

        public static void Update()
        {
            gammalMus = mus;
            gammaltTangentbord = tangentbord;
            mus = Mouse.GetState();
            tangentbord = Keyboard.GetState();
        }

        public static bool IsKeyPressed(Keys key)
        {
            if (tangentbord.IsKeyDown(key) && gammaltTangentbord.IsKeyUp(key))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
