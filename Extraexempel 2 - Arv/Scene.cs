﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Extraexempel_2___Arv
{
    class Scene
    {
        public List<Sprite> sprites = new List<Sprite>();
        
        public virtual void Initialize()
        {

        }

        public virtual void Update()
        {
            foreach (Sprite sprite in sprites)
            {
                sprite.Update();
            }
        }

        public virtual void Draw()
        {
            foreach (Sprite sprite in sprites)
            {
                Globals.spriteBatch.Draw(sprite.bild, sprite.rect, sprite.color);
            }
        }
    }
}
