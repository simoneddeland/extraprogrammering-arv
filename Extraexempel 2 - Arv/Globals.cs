﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extraexempel_2___Arv
{
    class Globals
    {
        public static ContentManager Content;
        public static SpriteBatch spriteBatch;
        public static Scene currentScene;

        public static void ChangeScene(Scene newScene)
        {
            currentScene = newScene;
            currentScene.Initialize();
        }
    }
}
