﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Extraexempel_2___Arv
{
    class GameScene : Scene
    {
        Texture2D ghostBild;
        Texture2D skeletonBild;

        public override void Draw()
        {
            base.Draw();
        }

        public override void Initialize()
        {
            ghostBild = Globals.Content.Load<Texture2D>("ghost");
            skeletonBild = Globals.Content.Load<Texture2D>("skeleton");
            base.Initialize();
        }

        public override void Update()
        {
            if (Input.mus.LeftButton == ButtonState.Pressed)
            {
                Ghost temp = new Ghost();
                temp.bild = ghostBild;
                temp.color = Color.Orange;
                temp.rect = new Rectangle(Input.mus.X, Input.mus.Y, 50, 50);
                sprites.Add(temp);
            }

            if (Input.mus.RightButton == ButtonState.Pressed)
            {
                Skeleton temp = new Skeleton();
                temp.bild = skeletonBild;
                temp.color = Color.Green;
                temp.rect = new Rectangle(Input.mus.X, Input.mus.Y, 50, 50);
                sprites.Add(temp);
            }

            if (Input.mus.LeftButton == ButtonState.Pressed && Input.mus.RightButton == ButtonState.Pressed)
            {
                Globals.ChangeScene(new GameScene());
            }
            base.Update();
        }
    }
}
